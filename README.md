This android app is to communicate with my totem via the HC-06 bluetooth module. 
Since the HC-06 is so unreliable, the Arduino code echos back what it received.
If it doesn't match, this app will retransmit. There's also a resend button in
case no response was received, though I should probably wait and retransmit
in that case.

It's pretty quick and dirty, though I am having fun making minor improvements /
adding features so it may end up looking something like a decent app. Right now
it does horrible things like hardcode MAC addresses.
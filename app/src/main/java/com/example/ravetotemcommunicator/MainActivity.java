package com.example.ravetotemcommunicator;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.os.Handler;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.text.method.ScrollingMovementMethod;
import android.bluetooth.BluetoothAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import me.aflak.bluetooth.interfaces.DeviceCallback;
import me.aflak.bluetooth.Bluetooth;

public class MainActivity extends AppCompatActivity {

    // Bluetooth
    private Bluetooth bluetooth;
    private BluetoothDevice device;
    private Boolean connectedToTotem = false;

    // Views
    private ImageButton totem;
    private TextView log;

    // Storage
    private String lastMessage;
    private Boolean sendSuccess = false;

    // SMS
    private Boolean smsEnabled = true;

    private final short MAX_MESSAGE_LENGTH = 32;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Ensure permissions are granted
        getPermissions();

        setContentView(R.layout.activity_main);

        // Get bluetooth adapter and register callbacks
        device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice("00:14:03:05:5A:C4");
        bluetooth = new Bluetooth(this);
        bluetooth.setCallbackOnUI(this);
        bluetooth.setDeviceCallback(deviceCallback);

        // Register the SMS listener
        SmsListener smsListener = new SmsListener() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
                    for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                        lastMessage = smsMessage.getMessageBody();
                        if (validateSMS(smsMessage)) {
                            sendMessage(smsMessage.getMessageBody());
                        }
                    }
                }
            }
        };

        // Set a filter to only receive bluetooth state changed events.
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);

        registerReceiver(smsListener, new IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION));
    }

    public void getPermissions() {

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.BLUETOOTH,
                android.Manifest.permission.BLUETOOTH_ADMIN,
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.RECEIVE_SMS,
                android.Manifest.permission.SEND_SMS
        };

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean validateSMS(SmsMessage sms) {
        SmsManager smsManager = SmsManager.getDefault();

        if(!connectedToTotem) {
            logMessage("Received SMS message, but not connected to totem. Ignoring.");
            return false;
        }

        if (sms.getOriginatingAddress().equals("2502587066")) {
            logMessage("Received SMS message from self. Skipping...");
            return false;
        }
        if (sms.getOriginatingAddress().length() < 10) {
            logMessage("Originating number is short, probably a bot. Ignoring");
            return false;
        }
        if(sms.getMessageBody().length() > MAX_MESSAGE_LENGTH) {
            logMessage("Received SMS that was too long from " + sms.getOriginatingAddress() + ". Sending informative response...");
            if (smsEnabled) {
                smsManager.sendTextMessage(sms.getOriginatingAddress(), null, "Sorry, that message is just too long for the totem's tiny brain! Try sending something " + MAX_MESSAGE_LENGTH + " characters or less!", null, null);
            }
            return false;
        }
        if(sms.getMessageBody().matches("[^ -~]*$")) {
            logMessage("Received SMS with invalid characters from " + sms.getOriginatingAddress() + ". Sending informative response...");
            if (smsEnabled) {
                smsManager.sendTextMessage(sms.getOriginatingAddress(), null, "Sorry, but your message contains some invalid characters! Only letters, numbers, and basic symbols please :)", null, null);
            }
            return false;
        }
        logMessage("Received valid message \"" + sms.getMessageBody() + "\" from " + sms.getOriginatingAddress() + ". Sending a thank you...");
        smsManager.sendTextMessage(sms.getOriginatingAddress(), null, "Thanks for the message! It's going up now :)", null, null);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Set up our TextViews
        log = findViewById(R.id.textViewLog);
        log.setMovementMethod(new ScrollingMovementMethod());

        // And our buttons
        totem = findViewById(R.id.imageButtonTotem);

        // Start our bluetooth device, attempt to connect to the rave totem
        bluetooth.onStart();
        reconnect();
    }

    public void reconnect() {
        // Attempt to enable the bluetooth adapter if it's disabled. The state change handler will try to connect once it's on
        if (!bluetooth.isEnabled()) {
            logMessage("Enabling bluetooth...");
            bluetooth.enable();
            return;
        } else { // Otherwise, just reconnect. You simply lost connection to the totem
            logMessage("Connecting to rave totem...");
            bluetooth.connectToDevice(device);
        }
    }

    // Our bluetooth adapter state change handler
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent){
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (bluetoothState) {
                    case BluetoothAdapter.STATE_ON:
                        logMessage("Bluetooth was enabled");
                        reconnect();
                }
            }
        }
    };

    public void OnClickTotem(View view){
        if (bluetooth.isConnected()) {
            logMessage("Disconnecting from rave totem...");
            bluetooth.disconnect();
        } else {
            reconnect();
        }
    }

    public void OnClickSendIt(View view) {

        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        lastMessage = editText.getText().toString();
                        sendMessage(editText.getText().toString());
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void OnClickToggleSMS(View view) {
        Button button = (Button)findViewById(R.id.toggleSMSButton);
        smsEnabled = !smsEnabled;
        if (smsEnabled) {
            logMessage("SMS replies have been enabled.");
            button.setText("DISABLE SMS REPLIES");
        } else {
            logMessage("SMS replies have been disabled");
            button.setText("ENABLE SMS REPLIES");
        }
    }


    public void sendMessage(final String message) {
        if (message.length() > MAX_MESSAGE_LENGTH) {
            logMessage("Message too long. discarding.");
            return;
        }

        if (bluetooth.isConnected()) {
            sendSuccess = false; // We're not sure if it has been successfully sent, set this to false so that we can check it once later
            bluetooth.send(message);
            logMessage("Sent message \"" + message + "\" to totem");

            // Start a timer to check if it received the response
            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (sendSuccess == false  && bluetooth.isConnected()) {
                        logMessage("Haven't received response from totem in 2000 millis, resending message \"" + message + "\"");
                        sendMessage(message);
                    }
                }
            }, 2000);

        } else {
            logMessage("Message not sent. Rave totem is not connected");
        }
    }

    // A button called it, need to have the view
    public void resendMessage(View view) {
        logMessage("Resending last SMS message \"" + lastMessage + "\"");
        sendMessage(lastMessage);
    }

    public void logMessage(String message) {
        log.append(message + "\n");
    }

    private DeviceCallback deviceCallback = new DeviceCallback() {
        @Override
        public void onDeviceConnected(BluetoothDevice device) {
            logMessage("Connected to rave totem");
            connectedToTotem = true;
            totem.setImageResource(R.drawable.ic_totem_on);
        }

        @Override
        public void onDeviceDisconnected(BluetoothDevice device, String message) {
            logMessage("Disconnected from rave totem");
            connectedToTotem = false;
            totem.setImageResource(R.drawable.ic_totem_off);
        }

        // When receiving a response, check to see if it matches lastMessage. If not, retransmit
        @Override
        public void onMessage(byte[] message) {
            String strMessage = new String(message); // Convert the message
            strMessage = strMessage.replaceAll("[^ -~]", "");
            if (!(strMessage.equals(lastMessage))) {
                logMessage("Received message \"" + strMessage + "\" does not match last sent text \"" + lastMessage + "\", retransmitting...");
                sendMessage(lastMessage);
            } else {
                logMessage("Received matching totem response \"" + strMessage + "\"");
                sendSuccess = true;
            }
        }

        @Override
        public void onError(int errorCode) {

        }

        @Override
        public void onConnectError(BluetoothDevice device, String message) {

        }
    };


}